package com.mcmacker4.blockland.gameloop;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import com.mcmacker4.blockland.handlers.DisplayHandler;
import com.mcmacker4.blockland.handlers.GLHandler;
import com.mcmacker4.blockland.states.StateManager;
import com.mcmacker4.blockland.texture.Textures;
import com.mcmacker4.blockland.util.Timer;

public class Loop {
	
	private StateManager manager;

	public Loop() {
		manager = new StateManager();
	}
	
	public void start() {
		DisplayHandler.create();
		GLHandler.create();
		
		init();
		
		while(!Display.isCloseRequested()) {
			
			if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) break;
			
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			Timer.update();
			
			update(Timer.getDelta());
			render();
			
			Display.update();
			Display.sync(1000);
			
		}
		
		DisplayHandler.destroy();
	}
	
	public void init() {
		Textures.load();
		manager.init();
	}
	
	public void update(int delta) {
		manager.update(delta);
	}
	
	public void render() {
		manager.render();
	}
	
}
