package com.mcmacker4.blockland.util;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;

public class Timer {
	
	private static long lastD = getTime();
	private static long delta = 0;
	private static long FPS = 0;
	
	public static void update() {
		calculateDelta();
		FPS = 1000 / delta;
		Display.setTitle(FPS + " fps");
	}
	
	public static long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public static void calculateDelta() {
		 long now = getTime();
		 delta = now - lastD;
		 if(delta == 0) delta = 1;
		 lastD = now;
	}
	
	public static int getFPS() {
		return (int) FPS;
	}
	
	public static int getDelta() {
		return (int) delta;
	}
	
}
