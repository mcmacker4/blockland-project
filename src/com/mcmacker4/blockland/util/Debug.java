package com.mcmacker4.blockland.util;

import com.mcmacker4.blockland.objects.AObject;

import static org.lwjgl.opengl.GL11.*;

public class Debug extends AObject {

	@Override
	public void init() {
		
	}

	@Override
	public void update(int delta) {
		
	}

	@Override
	public void render() {
		
		glColor4f(1f, 0f, 0f, 1f);
		glBegin(GL_LINES);
			glVertex3f(0f, 0f, 0f);
			glVertex3f(1000f, 0f, 0f);
		glEnd();
		
		glColor4f(0f, 1f, 0f, 1f);
		glBegin(GL_LINES);
			glVertex3f(0f, 0f, 0f);
			glVertex3f(0f, 1000f, 0f);
		glEnd();
		
		glColor4f(0f, 0f, 1f, 1f);
		glBegin(GL_LINES);
			glVertex3f(0f, 0f, 0f);
			glVertex3f(0f, 0f, 100f);
		glEnd();
	}
	
	@Override
	public String getName() {
		return "Debug";
	}

}
