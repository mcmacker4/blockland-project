package com.mcmacker4.blockland.main;

import com.mcmacker4.blockland.gameloop.Loop;

public class MainComponent {

	public static void main(String[] args) {
		
		Loop loop = new Loop();
		loop.start();
		
	}
	
}
