package com.mcmacker4.blockland.states;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;

import com.mcmacker4.blockland.objects.AObject;
import com.mcmacker4.blockland.player.Camera;
import com.mcmacker4.blockland.states.superclasses.AState;
import com.mcmacker4.blockland.util.Debug;
import com.mcmacker4.blockland.world.chunk.ChunkManager;

public class GameState extends AState {
	
	ArrayList<AObject> objects = new ArrayList<>();
//	Block grass = new BlockGrass(new Vector3f(0, 0, 0), null);
	
	@Override
	public void init() {
		Mouse.setGrabbed(true);
		objects.add(new Camera());
		objects.add(new ChunkManager());
		objects.add(new Debug());
		for(AObject o : objects)
			o.init();
	}

	@Override
	public void update(int delta) {
		for(AObject o : objects)
			o.update(delta);
	}

	@Override
	public void render() {
		for(AObject o : objects)
			o.render();
	}

	@Override
	public int getID() {
		return 1;
	}

	@Override
	public String getName() {
		return "Game State";
	}
	
}