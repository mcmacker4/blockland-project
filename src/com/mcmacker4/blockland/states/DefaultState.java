package com.mcmacker4.blockland.states;

import com.mcmacker4.blockland.states.superclasses.AState;

public class DefaultState extends AState {
	@Override
	public void init() {}
	@Override
	public void update(int delta) {}
	@Override
	public void render() {}
	@Override
	public int getID() {
		return 0;
	}
	@Override
	public String getName() {
		return "Default State";
	}
}
