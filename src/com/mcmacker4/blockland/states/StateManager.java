package com.mcmacker4.blockland.states;

import java.util.ArrayList;

import com.mcmacker4.blockland.states.superclasses.AState;
import com.mcmacker4.blockland.superclasses.IUpdatingClass;

public class StateManager implements IUpdatingClass {
	
	private static ArrayList<AState> states = new ArrayList<>();
	private static int currentState = 0;

	@Override
	public void init() {
		addState(new DefaultState());
		addState(new GameState());
		currentState = 1;
		states.get(currentState).init();
	}

	@Override
	public void update(int delta) {
		states.get(currentState).update(delta);
	}

	@Override
	public void render() {
		states.get(currentState).render();
	}
	
	public static void enterState(int state) {
		if(currentState == state) return;
		if(states.get(state).equals(null)) {
			System.err.println("State doesn't exist");
			return;
		}
		currentState = state;
		states.get(currentState).init();
	}
	
	private void addState(AState state) {
		int id = state.getID();
		for(AState s : states) {
			if(s.getID() == id) {
				System.err.println("Duplicate State or ID @ " + state.getName());
				return;
			}
		}
		states.add(id, state);
		System.out.println("State Added: '" + state.getName() + "'");
	}

}
