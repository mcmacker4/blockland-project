package com.mcmacker4.blockland.states.superclasses;

import com.mcmacker4.blockland.superclasses.IUpdatingClass;

public interface IState extends IUpdatingClass {
	
	public int getID();
	public String getName();
	
}
