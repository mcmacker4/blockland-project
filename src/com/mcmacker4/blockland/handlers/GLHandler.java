package com.mcmacker4.blockland.handlers;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;
import static com.mcmacker4.blockland.handlers.ConfigHandler.*;

import org.lwjgl.opengl.Display;

public class GLHandler {
	
	public static void create() {
		
		glViewport(0, 0, Display.getWidth(), Display.getHeight());
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(FOV, (int) Display.getWidth() / (int) Display.getHeight(), Z_NEAR, Z_FAR);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glEnable(GL_TEXTURE_2D);

		glClearColor(0.6f, 0.6f, 1f, 1f);
		
		glEnable(GL_DEPTH_TEST);
		
		glEnable(GL_CULL_FACE);
		
	}
	
}
