package com.mcmacker4.blockland.handlers;

public class ConfigHandler {

	//Available display modes to be printed on console using printDisplayModes function
	//Or running program with argument "printDisplayModes".
	public static int DISPLAY_MODE = 22;
	public static boolean FULLSCREEN = false;
	public static boolean PRINT_MODES = true;
	
	public static float FOV = 80f;
	public static float Z_NEAR = 0.1f;
	public static float Z_FAR = 100f;
	
}
