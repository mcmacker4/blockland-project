package com.mcmacker4.blockland.handlers;

import java.util.HashSet;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static com.mcmacker4.blockland.handlers.ConfigHandler.*;

public class DisplayHandler {
	
	private static DisplayMode[] modes;
	private static HashSet<Integer> frequencies = new HashSet<>();
	private static HashSet<Integer> widths = new HashSet<>();
	private static HashSet<Integer> heights = new HashSet<>();
	private static HashSet<Boolean> fullscreen = new HashSet<>();

	public static void create() {
		
//		if(ConfigHandler.PRINT_MODES) printDisplayModes();
//		
//		try {
//			modes = Display.getAvailableDisplayModes();
//			Display.setDisplayMode(modes[DISPLAY_MODE]);
//			Display.setFullscreen(FULLSCREEN);
//			Display.setTitle("BlockLand Project");
//			Display.create();
//			Keyboard.create();
//			Mouse.create();
//		} catch (LWJGLException e) {
//			e.printStackTrace();
//		}	
		
		try {
			Display.setDisplayMode(new DisplayMode(800, 800));
			Display.setTitle("BlockLand Project");
			Display.create();
			Keyboard.create();
			Mouse.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void destroy() {
		Display.destroy();
		Keyboard.destroy();
		Mouse.destroy();
	}
	
	public static void printDisplayModes() {
		try {
			modes = Display.getAvailableDisplayModes();
			for(int i = 0; i < modes.length; i++) {
				frequencies.add(modes[i].getFrequency());
				widths.add(modes[i].getWidth());
				heights.add(modes[i].getHeight());
				fullscreen.add(modes[i].isFullscreenCapable());
				
				System.out.println(
						"Mode " + i + ": " 
						+ modes[i].getFrequency() + "Hz " 
						+ "W: " + modes[i].getWidth() + " H: " 
						+ modes[i].getHeight() + " FS: " 
						+ modes[i].isFullscreenCapable()
				);
			}
 		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	public static int getWidth() {
		return modes[DISPLAY_MODE].getWidth();
	}
	
	public static int getHeight() {
		return modes[DISPLAY_MODE].getHeight();
	}
	
}
