package com.mcmacker4.blockland.player;

import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glRotated;
import static org.lwjgl.opengl.GL11.glTranslated;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.mcmacker4.blockland.objects.AObject;
import com.mcmacker4.blockland.util.Timer;
import com.mcmacker4.blockland.util.Vector3d;

public class Camera extends AObject {

	public static Vector3d pos = new Vector3d(0, 0, 0);
	public static Vector3d rot = new Vector3d(0, 0, 0);
	
	private static float speed = 0.01f;
	
	@Override
	public void init() {}
	@Override
	public void render() {}
	
	@Override
	public void update(int delta) {
		
		rot.x -= Mouse.getDY() * 0.15f;
		rot.y += Mouse.getDX() * 0.15f;
		
		if(rot.x > 90) rot.x = 90;
		if(rot.x < -90) rot.x = -90;
		
//		System.out.println(pos.x + " " + pos.y + " " + pos.z);
		
		if(rot.y > 360) rot.y -= 360;
		if(rot.y < 0) rot.y += 360;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_W)) goForward();
		if(Keyboard.isKeyDown(Keyboard.KEY_S)) goBackward();
		if(Keyboard.isKeyDown(Keyboard.KEY_A)) goLeft();
		if(Keyboard.isKeyDown(Keyboard.KEY_D)) goRight();
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) goDown();
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) goUp();
		
		glLoadIdentity();
		glRotated(rot.x, 1, 0, 0);
		glRotated(rot.y, 0, 1, 0);
		glTranslated(pos.x, pos.y, pos.z);
		
//		System.out.println(pos.x + " " + pos.y + " " + pos.z + " " + rot.x + " " + rot.y);
		
	}
	
	public static Vector3d forward() {
		
		Vector3d cam = new Vector3d(0, 0, 1);
		
//		cam.rotateX(Math.toRadians(-rot.x));
		cam.rotateY(Math.toRadians(-rot.y));
		
		return cam;
		
	}
	
	public static Vector3d lookAt() {
		Vector3d cam = new Vector3d(0, 0, 1);
		
		cam.rotateX(Math.toRadians(-rot.x));
		cam.rotateY(Math.toRadians(-rot.y));
		
		return cam;
	}
	
	public static void goForward() {
		double x = forward().x;
//		double y = forward().y;
		double z = forward().z;
		
		x *= speed;
//		y *= speed;
		z *= speed;
		
		int delta = Timer.getDelta();
		
		pos.x += x * delta;
//		pos.y += y * delta;
		pos.z += z * delta;
	}
	
	public static void goBackward() {
		double x = forward().x;
//		double y = forward().y;
		double z = forward().z;
		
		x *= speed;
//		y *= speed;
		z *= speed;
		
		int delta = Timer.getDelta();
		
		pos.x -= x * delta;
//		pos.y -= y * delta;
		pos.z -= z * delta;
	}
	
	public static void goLeft() {
		
		rot.y += 90;
		
		double x = forward().x;
//		double y = forward().y;
		double z = forward().z;
		
		x *= speed;
		z *= speed;
		
		int delta = Timer.getDelta();
		
		pos.x -= x * delta;
		pos.z -= z * delta;
		
		rot.y -= 90;
		
	}
	
	public static void goRight() {
		
		rot.y -= 90;
		
		double x = forward().x;
//		double y = forward().y;
		double z = forward().z;
		
		x *= speed;
		z *= speed;
		
		int delta = Timer.getDelta();
		
		pos.x -= x * delta;
		pos.z -= z * delta;
		
		rot.y += 90;
		
	}
	
	public static void goDown() {
		
		int delta = Timer.getDelta();
		pos.y += speed * delta;
		
	}
	
	public static void goUp() {
		
		int delta = Timer.getDelta();
		pos.y -= speed * delta;
		
	}
	@Override
	public String getName() {
		return "Camera";
	}

}
