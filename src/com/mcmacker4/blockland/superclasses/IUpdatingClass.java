package com.mcmacker4.blockland.superclasses;

public interface IUpdatingClass {

	public void init();
	public void update(int delta);
	public void render();
	
}
