package com.mcmacker4.blockland.world.superclasses.chunk;

import com.mcmacker4.blockland.superclasses.IUpdatingClass;
import com.mcmacker4.blockland.world.superclasses.block.ABlock;

public interface IChunk extends IUpdatingClass {
	
	public ABlock getBlockAt(int x, int y, int z);
	public void setBlock(ABlock block, int x, int y, int z);
	
}
