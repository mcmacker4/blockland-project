package com.mcmacker4.blockland.world.superclasses.block;

public interface IBlock {
	
	public boolean isSolid();
	public boolean isDraw();
	public boolean isTranslucent();
	public int texture();
	public void fixFaces();
	public void draw();
	
}
