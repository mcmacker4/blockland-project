package com.mcmacker4.blockland.world.block;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import org.lwjgl.util.vector.Vector3f;

import com.mcmacker4.blockland.texture.Textures;
import com.mcmacker4.blockland.world.chunk.ChunkManager;
import com.mcmacker4.blockland.world.superclasses.block.ABlock;

public class Block extends ABlock {
	
	String name = "AIR";
	
	static int TOP_TEX = 0;
	static int SIDE_TEX = 1;
	static int BOT_TEX = 2;
	
	private static final int FRONT = 0;
	private static final int BACK = 1;
	private static final int RIGHT = 2;
	private static final int LEFT = 3;
	private static final int TOP = 4;
	private static final int BOTTOM = 5;
	
	private int VBO;
	
	private boolean[] drawFaces = new boolean[6];
	
	private Vector3f pos;
	private ChunkManager manager;
	
	public Block(Vector3f pos, ChunkManager manager) {
		this.pos = pos;
		this.manager = manager;
		
		createVBO();
		
	}

	@Override
	public int texture() {
		return Textures.DIRT;
	}
	
	@Override
	public boolean isDraw() {
		return false;
	}
	
	@Override
	public boolean isSolid() {
		return false;
	}
	
	@Override
	public boolean isTranslucent() {
		return true;
	}
	
	@Override
	public void fixFaces() {
		setDrawFace(FRONT, manager.getBlockAt(pos.x, pos.y, pos.z - 1).isTranslucent());
		setDrawFace(BACK, manager.getBlockAt(pos.x, pos.y, pos.z + 1).isTranslucent());
		setDrawFace(RIGHT, manager.getBlockAt(pos.x + 1, pos.y, pos.z).isTranslucent());
		setDrawFace(LEFT, manager.getBlockAt(pos.x - 1, pos.y, pos.z).isTranslucent());
		setDrawFace(TOP, manager.getBlockAt(pos.x, pos.y + 1, pos.z).isTranslucent());
		setDrawFace(BOTTOM, manager.getBlockAt(pos.x, pos.y - 1, pos.z).isTranslucent());
	}
	
	private void setDrawFace(int face, boolean draw) {
		drawFaces[face] = draw;
	}
	
	private void createVBO() {
		
		ByteBuffer buffer = ByteBuffer.allocateDirect(vertices.length * 4);
		buffer.order(ByteOrder.nativeOrder());
		FloatBuffer fbuffer = buffer.asFloatBuffer();
		fbuffer.put(vertices);
		fbuffer.flip();
		fbuffer.position(0);
		
		VBO = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, fbuffer, GL_STATIC_DRAW);
		
	}
	
	@Override
	public void draw() {
		
		glPushMatrix();
		
			glColor4f(1f, 1f, 1f, 1f);
		
			glTranslatef(pos.x, pos.y, pos.z);
			
			glBindTexture(GL_TEXTURE_2D, texture());
			
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glVertexPointer(3, GL_FLOAT, 5 * 4, 0);
			glTexCoordPointer(2, GL_FLOAT, 5 * 4, 3 * 4);
			
			glDrawArrays(GL_QUADS, 0, 24);
			
		glPopMatrix();
		
	}
	
	private float[] vertices = {
			//Front		//Textures
			0, 0, 0,	0.5f, 0.5f,
			0, 1, 0,	0.5f, 0f,
			1, 1, 0,	1f, 0f,
			1, 0, 0,	1f, 0.5f,
			//Back
			0, 0, 1,	1f, 0.5f,
			1, 0, 1,	0.5f, 0.5f,
			1, 1, 1,	0.5f, 0f,
			0, 1, 1,	1f, 0f,
			//Top
			0, 1 ,0,	0f, 1f,
			0, 1, 1,	0f, 0.5f,
			1, 1, 1,	0.5f, 0.5f,
			1, 1, 0,	0.5f, 1f,
			//Bottom
			0, 0, 0,	0f, 0f,
			1, 0, 0,	0.5f, 0f,
			1, 0, 1,	0.5f, 0.5f,
			0, 0, 1,	0f, 0.5f,
			//Left
			0, 0, 0,	1f, 0.5f,
			0, 0, 1,	0.5f, 0.5f,
			0, 1, 1,	0.5f, 0f,
			0, 1, 0,	1f, 0f,
			//Right
			1, 0, 0,	0.5f, 0.5f,
			1, 1, 0,	0.5f, 0f,
			1, 1, 1,	1f, 0f,
			1, 0, 1,	1f, 0.5f
	};
	
	public float[] FACE_FRONT = {
			0, 0, 0,
			0, 1, 0,
			1, 1, 0,
			1, 0, 0
	};
	
	public float[] FACE_BACK = {
			0, 0, 1,
			1, 0, 1,
			1, 1, 1,
			0, 1, 1
	};
	
	public float[] FACE_TOP = {
			0, 1 ,0,
			0, 1, 1,
			1, 1, 1,
			1, 1, 0
	};
	
	public float[] FACE_BOTTOM = {
			0, 0, 0,
			1, 0, 0,
			1, 0, 1,
			0, 0, 1
	};
	
	public float[] FACE_LEFT = {
			0, 0, 0,
			0, 0, 1,
			0, 1, 1,
			0, 1, 0
	};
	
	public float [] FACE_RIGHT = {
			1, 0, 0,
			1, 1, 0,
			1, 1, 1,
			1, 0, 1
	};

}
