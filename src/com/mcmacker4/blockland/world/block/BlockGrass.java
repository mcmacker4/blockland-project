package com.mcmacker4.blockland.world.block;

import org.lwjgl.util.vector.Vector3f;

import com.mcmacker4.blockland.objects.IObject;
import com.mcmacker4.blockland.texture.Textures;
import com.mcmacker4.blockland.world.chunk.ChunkManager;

public class BlockGrass extends Block {
	
	public BlockGrass(Vector3f pos, ChunkManager manager) {
		super(pos, manager);
	}
	
	@Override
	public boolean isSolid() {
		return true;
	}
	
	@Override
	public int texture() {
		return Textures.GRASS;
	}
	
	@Override
	public boolean isTranslucent() {
		return false;
	}
	
	@Override
	public boolean isDraw() {
		return true;
	}

}
