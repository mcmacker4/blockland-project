package com.mcmacker4.blockland.world.block;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

public class BlockRender {

	public ArrayList<Float> vertices = new ArrayList<>();
	private int VBO;
	
	private int texture;
	
	private boolean changed = false;
	
	public BlockRender(int texture) {
		
	}
	
	public void add(float[] vertx, int texture) {
		for(int i = 0; i < vertx.length; i++) {
			vertices.add(vertx[i]);
		}
		changed = true;
	}
	
	public void genBuffer() {
		
		if(changed) {
		
			float[] vertx = new float[vertices.size()];
			int i = 0;
			for(Float f : vertices) {
				vertx[i++] = (f == null) ? f : 0;
			}
			
			ByteBuffer buffer = ByteBuffer.allocateDirect(vertx.length * 4);
			buffer.order(ByteOrder.nativeOrder());
			FloatBuffer fbuffer = buffer.asFloatBuffer();
			fbuffer.put(vertx);
			fbuffer.flip();
			fbuffer.position(0);
			
			int VBO = glGenBuffers();
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, fbuffer, GL_STATIC_DRAW);
					
		}
		
		glBindTexture(GL_TEXTURE_2D, texture);
		
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glVertexPointer(3, GL_FLOAT, 5 * 4, 0);
		glTexCoordPointer(2, GL_FLOAT, 5 * 4, 3);
		
	}
	
}
