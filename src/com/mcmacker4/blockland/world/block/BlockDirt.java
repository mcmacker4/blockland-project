package com.mcmacker4.blockland.world.block;

import org.lwjgl.util.vector.Vector3f;

import com.mcmacker4.blockland.texture.Textures;
import com.mcmacker4.blockland.world.chunk.ChunkManager;

public class BlockDirt extends Block {
	
	public BlockDirt(Vector3f pos, ChunkManager manager) {
		super(pos, manager);
	}
	
	@Override
	public boolean isSolid() {
		return true;
	}
	
	public int texture() {
		return Textures.DIRT;
	}
	
	@Override
	public boolean isTranslucent() {
		return false;
	}
	
	@Override
	public boolean isDraw() {
		return true;
	}

}
