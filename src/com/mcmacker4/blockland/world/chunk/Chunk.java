package com.mcmacker4.blockland.world.chunk;

import static org.lwjgl.opengl.GL11.GL_COMPILE_AND_EXECUTE;
import static org.lwjgl.opengl.GL11.glCallList;
import static org.lwjgl.opengl.GL11.glEndList;
import static org.lwjgl.opengl.GL11.glGenLists;
import static org.lwjgl.opengl.GL11.glNewList;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.mcmacker4.blockland.world.block.Block;
import com.mcmacker4.blockland.world.block.BlockDirt;
import com.mcmacker4.blockland.world.block.BlockGrass;
import com.mcmacker4.blockland.world.superclasses.block.ABlock;
import com.mcmacker4.blockland.world.superclasses.chunk.AChunk;

public class Chunk extends AChunk {
	
	private int glList = -1;
	
	public static final Vector3f size = new Vector3f(4, 5, 4);
	private Vector2f pos;
	
	private ABlock[][][] blocks = new ABlock[(int) size.x][(int) size.y][(int) size.z];
	
	private ChunkManager manager;
	
	public Chunk(Vector2f pos, ChunkManager manager) {
		this.pos = pos;
		this.manager = manager;
	}

	@Override
	public void init() {
		
		create();
//		createList();
		
	}
	
	private void create() {
		
		for(int z = 0; z < size.z; z++) {
			for(int y = 0; y < size.y; y++) {
				for(int x = 0; x < size.x; x++) {
					if(y == size.y - 1) {
						blocks[x][y][z] = new BlockGrass(new Vector3f(x + size.x * pos.x, y, z + size.z * pos.y), manager);
					} else {
						blocks[x][y][z] = new BlockDirt(new Vector3f(x + size.x * pos.x, y, z + size.z * pos.y), manager);
					}
				}
			}
		}
		
		for(int z = 0; z < size.z; z++) {
			for(int y = 0; y < size.y; y++) {
				for(int x = 0; x < size.x; x++) {
					blocks[x][y][z].fixFaces();
				}
			}
		}
		
	}
	
	private void createList() {
		
		glList = glGenLists(1);
		glNewList(glList, GL_COMPILE_AND_EXECUTE);
		
		for(int z = 0; z < size.z; z++) {
			for(int y = 0; y < size.y; y++) {
				for(int x = 0; x < size.x; x++) {
					blocks[x][y][z].draw();
				}
			}
		}
		
		glEndList();
		
	}

	@Override
	public void update(int id) {
		
	}

	@Override
	public void render() {
		
//		glCallList(glList);
		
		for(int z = 0; z < size.z; z++) {
			for(int y = 0; y < size.y; y++) {
				for(int x = 0; x < size.x; x++) {
					blocks[x][y][z].draw();
				}
			}
		}
		
	}

	@Override
	public ABlock getBlockAt(int x, int y, int z) {
		try {
			return blocks[x][y][z];
		} catch (Exception e) {
			return new Block(new Vector3f(-1, -1, -1), null);
		}
	}
	
	@Override
	public void setBlock(ABlock block, int x, int y, int z) {
		
	}

}
