package com.mcmacker4.blockland.world.chunk;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.mcmacker4.blockland.objects.AObject;
import com.mcmacker4.blockland.world.block.Block;
import com.mcmacker4.blockland.world.superclasses.block.ABlock;
import com.mcmacker4.blockland.world.superclasses.chunk.AChunk;

public class ChunkManager extends AObject {
	
	private final Vector2f SIZE = new Vector2f(2, 2);
	
	private AChunk[][] chunks = new AChunk[(int) SIZE.x][(int) SIZE.y];

	@Override
	public void init() {
		
		for(int y = 0; y < SIZE.y; y++) {
			for(int x = 0; x < SIZE.x; x++) {
				chunks[x][y] = new Chunk(new Vector2f(x, y), this);
				chunks[x][y].init();
			}
		}
		
	}

	@Override
	public void update(int delta) {}

	@Override
	public void render() {
		for(int y = 0; y < SIZE.y; y++) {
			for(int x = 0; x < SIZE.x; x++) {
				chunks[x][y].render();
			}
		}
	}

	@Override
	public String getName() {
		return "Chunk Manager";
	}
	
	public ABlock getBlockAt(float x, float y, float z) {
		
		int XX = (int) (x / Chunk.size.x);
		int ZZ = (int) (z / Chunk.size.z);
		
		int xx = (int) (x % Chunk.size.x);
		int yy = (int) (y % Chunk.size.y);
		int zz = (int) (z % Chunk.size.z);
		
		try {
			return chunks[XX][ZZ].getBlockAt(xx, yy, zz);
		} catch (Exception e) {
			return new Block(new Vector3f(-1, -1, -1), this);
		}
		
	}

}
