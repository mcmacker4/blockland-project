package com.mcmacker4.blockland.texture;

public class Textures {
	
	public static int GRASS;
	public static int DIRT;

	public static void load() {
		
		GRASS = Texture.get("blocks/block_grass.png");
		DIRT = Texture.get("blocks/block_dirt.png");
		
	}
	
}
