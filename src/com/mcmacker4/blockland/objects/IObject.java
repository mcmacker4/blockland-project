package com.mcmacker4.blockland.objects;

import com.mcmacker4.blockland.superclasses.IUpdatingClass;

public interface IObject extends IUpdatingClass {
	public String getName();
}
